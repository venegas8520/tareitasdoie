import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  publicaciones: any = [
    { isFavorite: false , favoriteNumber: 10, title: 'Ayuda en matemáticas', user: 'Victor', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 40, title: 'Ayuda en quimica', user: 'Alfredo', texto: 'Lorem ipsum dolor psum dolor psum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: true ,  favoriteNumber: 2 , title: 'Me sale error', user: 'Cinthya', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 31, title: 'Se quemó mi platillo', user: 'Alberto', texto: 'Lorem ipsum dolor sitrem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: true ,  favoriteNumber: 10, title: 'No se que significa esto', user: 'Gilberto', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 12, title: '¿Que colores uso?', user: 'Betzaira', texto: 'Lorem ipsum dolor sit amet consectet sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 13, title: 'como crakeo Photoshop', user: 'Celina', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: true ,  favoriteNumber: 10, title: 'Ayuda en Excel', user: 'Jorge', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 34, title: '¿Como programar?', user: 'Javier', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimoadipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 1 , title: 'Me sale la pantalla azul', user: 'Gerardo', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: true ,  favoriteNumber: 23, title: 'Se quemó mi RAM', user: 'Esmeralda', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
    { isFavorite: false , favoriteNumber: 54, title: 'Perdí mis archivos', user: 'Isabel', texto: 'Lorem ipsum dolor sit amet consectetur adipisicing elisit amet consectetur adipisicing elisit amet consectetur adipisicing elisit amet consectetur adipisicing elisit amet consectetur adipisicing elisit amet consectetur adipisicing elisit amet consectetur adipisicing elit. Pariatur dignissimos, unde aut quod illo voluptate ducimus laudantium cum molestiae magni eaque.' },
  ]
  // constructor
  constructor() { }

  onClick(i){
    console.log(i);
    if (this.publicaciones[i].isFavorite === true) {
      this.publicaciones[i].isFavorite = false;
      this.publicaciones[i].favoriteNumber = this.publicaciones[i].favoriteNumber - 1;
    } else {
      this.publicaciones[i].isFavorite = true;
      this.publicaciones[i].favoriteNumber = this.publicaciones[i].favoriteNumber + 1;
    }
  }

  // ngOnInit
  ngOnInit(): void {
  }

}
